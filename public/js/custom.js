$(document).ready(function(){
    $.ajax({
        url : 'https://api.myjson.com/bins/b6wq3',
        type: 'GET',
        dataType: 'json',
        success: function(result){
            var movie = result.results;
            var url_image = 'https://image.tmdb.org/t/p/w500';
            $.each(movie, function(i, data){
                $('#movie-list').append(`
                <div class="col-12">
                    <div class="card mb-3">
                    <div class="row no-gutters">
                        <div class="col-md-4">
                        <img src="`+ url_image +data.poster_path + `" class="card-img">
                        </div>
                        <div class="col-md-8">
                        <div class="card-body">
                            <h5 class="card-title">`+ data.title  +`</h5>
                            <p class="card-text">Language : `+ data.original_language +`</p>
                            <p class="card-text">Popularity : `+ data.popularity +`</p>
                            <p class="card-text">Release Date : ` + data.release_date + `</p>
                            <p class="card-text">Vote Average : `+ data.vote_average +`</p>
                            <p class="card-text">Description :</p>
                            <p class="card-text">`+ data.overview +`</p>
                        </div>
                        </div>
                    </div>
                    </div>
                </div>
                `);
            })
        }
    });
});